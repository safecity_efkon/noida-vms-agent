﻿using NoidaVmsAgent.Extensions;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;

namespace NoidaVmsAgent
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                FileWatcher();
            }
            catch (Exception ex)
            {
                Loggers.WriteErrorLogs2TextFile(nameof(OnChanged), ex.Message, ex.HResult.ToString());
            }
            
        }

        protected override void OnStop()
        {

        }

        public static void FileWatcher()
        {
            var watch = new FileSystemWatcher();
            watch.Path = ConfigurationManager.AppSettings.Get("FolderPath");
            watch.Filter = "*.txt";
            watch.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite; //more options
            watch.Changed += new FileSystemEventHandler(OnChanged);
            watch.EnableRaisingEvents = true;
        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            try
            {
                if (e.Name.Equals(ConfigurationManager.AppSettings.Get("FileName")))
                {
                    
                    if(File.Exists(e.FullPath))
                    {
                        string[] cmds = File.ReadAllLines(e.FullPath);
                        bool _result = false;
                        for (int i = 0; i < cmds.Length; i++)
                        {
                            Loggers.WriteLogs2TextFile(e.FullPath,"=======("+ @cmds[i]+")");
                            _result = PassCmdToApp(@cmds[i]);
                            if (_result)
                            {
                                File.Move(e.FullPath, Path.Combine(ConfigurationManager.AppSettings.Get("MoveFolder"),   DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")+ "_" + Path.GetFileName(e.FullPath)));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Loggers.WriteErrorLogs2TextFile(nameof(OnChanged), ex.Message, ex.StackTrace);
            }
        }

        public static bool PassCmdToApp(string command)
        {
            try
            {
                ProcessStartInfo process = new ProcessStartInfo();
                process.FileName = @ConfigurationManager.AppSettings.Get("AppPath");
                process.Arguments = @command;
                using (Process exeProcess = Process.Start(process))
                {
                    exeProcess.ErrorDataReceived += ExeProcess_ErrorDataReceived;
                    exeProcess.WaitForExit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Loggers.WriteErrorLogs2TextFile(nameof(PassCmdToApp), ex.Message, ex.StackTrace);
                return false;
            }

        }

        private static void ExeProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Loggers.WriteErrorLogs2TextFile(nameof(ExeProcess_ErrorDataReceived), e.Data, string.Empty);
        }
    }
}
