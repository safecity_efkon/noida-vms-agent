﻿using System.ComponentModel;

namespace NoidaVmsAgent
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
            serviceInstaller1.Description = "VMS Command Grabber Agent";
            serviceInstaller1.DisplayName = "VMSCommandAgent";
        }
    }
}
