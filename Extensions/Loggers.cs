﻿using System;
using System.IO;

namespace NoidaVmsAgent.Extensions
{
    public static class Loggers
    {
        public static async void WriteErrorLogs2TextFile(string methodName, string hashCode = "", string statusCode = "")
        {
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "LogsFile\\";
            string strValue = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "----Method Name:" + methodName + "----status code: " + hashCode + " " + statusCode;
            if (!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);
            }
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(filepath, DateTime.Now.ToString("yyyy-MM-dd") + "ErrorLogs.txt"), append: true))
            {
                await outputFile.WriteAsync(strValue + Environment.NewLine);
            }
        }

        public static async void WriteLogs2TextFile(string message, string Details)
        {
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "LogsFile\\";
            string strValue = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "----Message:" + message + "----Details: " + Details;
            if (!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);
            }
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(filepath, DateTime.Now.ToString("yyyy-MM-dd") + "Logs.txt"), append: true))
            {
                await outputFile.WriteAsync(strValue + Environment.NewLine);
            }
        }
    }
}
